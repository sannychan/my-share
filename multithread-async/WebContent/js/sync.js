var sleepLong = 5000;

var sync = function() {
	$.ajax({
		type : 'GET',
		url : 'sync',
		async : false,
		cache:false,
		data:{sleep:sleepLong,type:'sync'},
		success : function(result) {
			alert(JSON.stringify(result));
		},
		dataType : 'json'
	});

}

var async = function() {
	$.ajax({
		type : 'GET',
		url : 'sync',
		async : true,
		cache:false,
		data:{sleep:sleepLong,type:'async'},
		success : function(result) {
			alert(JSON.stringify(result));
		},
		dataType : 'json'
	});
}



var callback = function(){
	$.ajax({
		type : 'GET',
		url : 'sync',
		async : true,
		cache:false,
		data:{sleep:sleepLong,type:'async'},
		success : function(result) {
			$('#callback').html(JSON.stringify(result));
		},
		dataType : 'json'
	});
	setInterval(function(){
		console.log("html:"+$('#callback').html());
	}, 500)
}

var log = function(){
	var $li = $('<li>' + new Date().getMilliseconds() + '</li>');
	$li.appendTo($('#log'));
}