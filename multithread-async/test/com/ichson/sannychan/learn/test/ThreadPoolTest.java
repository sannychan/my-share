package com.ichson.sannychan.learn.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

public class ThreadPoolTest {
	
	private static Log logger = LogFactory.getLog(ThreadPoolTest.class);

	@Test
	public void test_newFixedThreadPool() throws InterruptedException {
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(10);
		newFixedThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("newFixedThreadPool");
			}
		});
		Thread.sleep(Long.MAX_VALUE);
	}

	@Test
	public void test_newSingleThreadExecutor() throws InterruptedException {
		ExecutorService newFixedThreadPool = Executors.newSingleThreadExecutor();
		newFixedThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				System.out.println("newSingleThreadExecutor");
			}
		});
		Thread.sleep(Long.MAX_VALUE);
	}

	@Test
	public void test_newCachedThreadPool() throws InterruptedException {
		ExecutorService newFixedThreadPool = Executors.newCachedThreadPool();
		newFixedThreadPool.execute(new Runnable() {
			@Override
			public void run() {
				logger.info("newCachedThreadPool");
			}
		});
		Thread.sleep(Long.MAX_VALUE);
	}

	@Test
	public void test_newSingleThreadScheduledExecutor() throws InterruptedException {
		ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor();
		Runnable task = new Runnable() {
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("newFixedThreadPool");
			}
		};
		//按固定的频率来执行某项计划， 不受计划执行时间的影响
		//newSingleThreadScheduledExecutor.scheduleAtFixedRate(task, 2, 2, TimeUnit.SECONDS);
		//按固定的等待时间来执行某项计划，受计划执行时间的影响
		//newSingleThreadScheduledExecutor.scheduleWithFixedDelay(task, 2, 2, TimeUnit.SECONDS);
		//单次执行
		//newSingleThreadScheduledExecutor.schedule(task, 2, TimeUnit.SECONDS);
		Thread.sleep(Long.MAX_VALUE);
	}
}
