package com.ichson.sannychan.learn.test;

import org.junit.Test;

import com.ichson.sannychan.learn.server.Solutions;

public class Client {

	String url = "http://localhost:8080/learn/clockin";

	public static final int machine_count = 400; // 打卡机数量
	public static final int submit_times = 100; // 数据提交次数

	// 方案一
	@Test
	public void test_solution_one() throws Exception {
		String url = getRequestUrl(Solutions.one.name());
		submit(url);
	}

	// 方案二
	@Test
	public void test_solution_two() throws Exception {
		String url = getRequestUrl(Solutions.two.name());
		submit(url);
	}

	// 方案上
	@Test
	public void test_solution_three() throws Exception {
		String url = getRequestUrl(Solutions.three.name());
		submit(url);
	}

	private void submit(String url) {
		for (int i = 0; i < machine_count; i++) {
			new Thread(new Request(url)).start();
		}

		try {
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private String getRequestUrl(String solution) {
		return url + "?solution=" + solution;
	}
}
