package com.ichson.sannychan.learn.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.sun.org.apache.bcel.internal.generic.NEW;

public class HttpUtils {
	
	private static String request_body = null;
	
	
	static{
		prepareContent();
	}

	public static String request(String url) throws Exception {
		URL urlObj = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
		connection.setConnectTimeout(30000);  
		connection.setReadTimeout(30000); 
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(true);
		connection.setRequestProperty("Content-Type", "text/plain");
		DataOutputStream out = new DataOutputStream(connection.getOutputStream());
		out.writeBytes(request_body);
		out.flush();
		out.close(); 
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		StringBuffer resultBuffer = new StringBuffer();
		String line ="";
		while ((line = reader.readLine()) != null) {
			resultBuffer.append(line);
		}
		reader.close();
		connection.disconnect();
		return resultBuffer.toString();
	}
	
	
	private static void prepareContent(){
		StringBuilder sb =new StringBuilder();
		for(int i = 0 ; i < 1024*102 ; i++){
			sb.append("a");
		}
		request_body =  sb.toString(); 
		System.out.println(request_body.getBytes().length);
	}
}
