package com.ichson.sannychan.learn.test;

public class Request implements Runnable {

	private String url;

	public Request(String url) {
		this.url = url;
	}

	@Override
	public void run() {
		long cost = 0;
		int errorTimes = 0;
		for (int i = 0; i < Client.submit_times; i++) {
			String response;
			try {
				long start = System.currentTimeMillis();
				response = HttpUtils.request(url);
				long end = System.currentTimeMillis();
				cost += (end - start);
				//System.out.println(response + ",cost:" + (end - start) + "ms");
			} catch (Exception e) {
				//e.printStackTrace();
				errorTimes++;
			}
		}
		System.out.println("平均耗时：" + (cost / Client.submit_times) + "ms,错误次数：" + errorTimes);

	}

}
