package com.ichson.sannychan.learn.service;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolService {
	public static BlockingQueue queue = null;
	public static ThreadPoolExecutor threadPoolExecutor = null;
	
	
	static{
		init();
	}
	
	private static void init(){
		queue = new ArrayBlockingQueue(500);
		threadPoolExecutor = new ThreadPoolExecutor(10,
                100,
                60,
                TimeUnit.SECONDS,
                queue,
                new LearnThreadFactory(),
                new LearnRejectedExecutionHandler());
	}
	
	
//	任务拒绝策略
//	当线程池的任务缓存队列已满并且线程池中的线程数目达到maximumPoolSize，如果还有任务到来就会采取任务拒绝策略，通常有以下四种策略：
//	ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
//	ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
//	ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务（重复此过程）
//	ThreadPoolExecutor.CallerRunsPolicy：由调用线程处理该任务

}
