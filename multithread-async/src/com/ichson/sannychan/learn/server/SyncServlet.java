package com.ichson.sannychan.learn.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;



@WebServlet(urlPatterns={"/sync"})
public class SyncServlet extends HttpServlet{

	private static final long serialVersionUID = -2822235621577288881L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String sleep = request.getParameter("sleep");
		String type = request.getParameter("type");
		long sleepLong = Long.parseLong(sleep);
		System.out.println("sleepLong:"+sleepLong);
		PrintWriter out = response.getWriter();
		try {
			Thread.sleep(sleepLong);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Map map = new HashMap();
		map.put("result", "ok");
		map.put("sleep", sleep);
		map.put("type", type);
		out.print(JSON.toJSONString(map));
	}
	
}
