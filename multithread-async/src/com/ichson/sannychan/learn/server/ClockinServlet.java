package com.ichson.sannychan.learn.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;


@WebServlet(urlPatterns={"/clockin"}) 
public class ClockinServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8121197210927922900L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String solution = req.getParameter("solution");
		byte[] body = new byte[req.getContentLength()];
		IOUtils.read(req.getInputStream(), body);
		if(Solutions.one.name().equals(solution)){
			new SolutionOne().execute();
		}else if(Solutions.two.name().equals(solution)){
			new SolutionTwo().execute();
		}else if(Solutions.three.name().equals(solution)){
			new SolutionThree().execute();
		}
		resp.setContentType("text/html;charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write("It is ok!");
	}
	
	

}
