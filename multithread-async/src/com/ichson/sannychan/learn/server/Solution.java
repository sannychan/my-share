package com.ichson.sannychan.learn.server;

public abstract class Solution {
	
	protected static long sleepTime =  500;

	public abstract void execute();
	
}
